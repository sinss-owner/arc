//
//  NoneARCViewController.h
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoneARCViewControllerDelegate;
@interface NoneARCViewController : UIViewController

@property (nonatomic, weak) id <NoneARCViewControllerDelegate> delegate;

@end


@protocol NoneARCViewControllerDelegate <NSObject>

- (void)noneArcViewDidDismiss;

@end
