//
//  NSArray+append.m
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "NSArray+append.h"

@implementation NSArray (append)

- (NSString*)toString
{
    NSMutableString *str = [NSMutableString string];
    for (NSString *item in self)
    {
        [str appendString:item];
    }
    return [NSString stringWithString:str];
}

@end
