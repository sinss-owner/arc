//
//  NoneARC+toString.h
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "NoneARC.h"

@interface NoneARC (toString)

- (void)toString;

@end
