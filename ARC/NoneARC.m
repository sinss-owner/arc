//
//  NoneARC.m
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "NoneARC.h"

@implementation NoneARC

+ (instancetype)newObject
{
    return [[NoneARC alloc] init];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _objectId = [[NSString alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_objectId release];
    [super dealloc];
}

@end
