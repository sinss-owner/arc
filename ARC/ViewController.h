//
//  ViewController.h
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoneARC;
@interface ViewController : UIViewController

@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) NoneARC *obj;

@end


