//
//  NoneARC.h
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface NoneARC : NSObject

@property (nonatomic, retain) NSString *objectId;

+ (instancetype)newObject;

@end
