//
//  NoneARCViewController.m
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "NoneARCViewController.h"
#import "NoneARC.h"
#import "NSArray+append.h"

@interface NoneARCViewController ()

@property (nonatomic, retain) NoneARC *noneArc;
@property (nonatomic, retain) NSArray *array;
@property (nonatomic, retain) NSDictionary *dict;

@end

@implementation NoneARCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    _noneArc = [[NoneARC alloc] init];
//    NSLog(@"retain count: %li", [_noneArc retainCount]);
//    //[_noneArc retain];
//    NSLog(@"retain count: %li", [_noneArc retainCount]);
//    
//    _array = [NSArray arrayWithObjects:@"1", @"2", @"3", nil];
//    NSLog(@"array retain count: %li", [_array retainCount]);
//    [_array retain];
//    NSLog(@"array retain count: %li", [_array retainCount]);
//    [_array release];
//    NSLog(@"array retain count: %li", [_array retainCount]);
//    
//    NSLog(@"array: %@", [_array toString]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(noneArcViewDidDismiss)])
        {
            [self.delegate noneArcViewDidDismiss];
        }
    }];
}

- (IBAction)open:(id)sender
{
    NoneARCViewController *vc = [[NoneARCViewController alloc] initWithNibName:@"NoneARCViewController" bundle:nil];
//    [vc retain];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
