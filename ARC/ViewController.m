//
//  ViewController.m
//  ARC
//
//  Created by leo.chang on 03/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "ViewController.h"
#import "NoneARCViewController.h"
#import "NoneARC.h"

@interface ViewController () <NoneARCViewControllerDelegate>

@property (nonatomic, strong) NSArray *xxx;

- (void)method;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NoneARC *obj = [NoneARC newObject];
    [obj toString];
    
    NSArray *item = @[@"1", @"2", @"3"];
    NSDictionary *dict = @{@"key":@"value", @"key2": @"value2"};
    [item toString];
    NSLog(@"%@", [item toString]);
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)open:(id)sender
{
    NoneARCViewController *vc = [[NoneARCViewController alloc] initWithNibName:@"NoneARCViewController" bundle:nil];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)noneArcViewDidDismiss
{
    NSLog(@"None ARC did dismissed");
}

- (void)dealloc
{
    
}

@end
